/**
* The Pizza class stores information about a single pizza.
*/
class Pizza
{
  private boolean pepperoni, sausage, mushrooms;
  private char size;
  
  // Constructors
  public Question2Pizza()
  {
    // Default pizza settings
    pepperoni = false;
    sausage = false;
    mushrooms = false;
    size = 's';
  }
  
  public Question2Pizza(char size, boolean pepperoni,
  boolean sausage, boolean mushrooms)
  {
    this.size = size;
    this.pepperoni = pepperoni;
    this.sausage = sausage;
    this.mushrooms = mushrooms;
  }
  
  /**
  * getSize
  * @return char Returns the size of the pizza
  */
  public char getSize()
  {
    return this.size;
  }
  
  
  /**
  * getNumToppings
  * @return int Returns the number of toppings on the pizza.
  */
  public int getNumToppings()
  {
    int num=0;
    if (pepperoni) num++;
    if (sausage) num++;
    if (mushrooms) num++;
    return num;
  }
}