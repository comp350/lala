/**
* The PizzaOrder class stores information about a pizza order.
*/
class PizzaOrder
{
  private static final int MAXPIZZAS = 100;
  private int numPizzas;
  private Pizza[] pizzas;
  
  public PizzaOrder()
  {
    pizzas = new Pizza[MAXPIZZAS];
    numPizzas = 0;
  }
  
  /**
  * addPizzaToOrder
  * Creates a new pizza of the specified type and
  * appends it to the pizza array.
  */
  public void addPizzaToOrder(char size, boolean pepperoni,
  boolean sausage, boolean mushrooms)
  {
    if (numPizzas == MAXPIZZAS)
    {
      // Would be better to throw an exception, but left off for simplicity
      System.out.println("No more pizzas allowed in the order.");
      return;
    }
    Pizza newpie = new Pizza(size, pepperoni, sausage, mushrooms);
    // Store new pizza in array and increment number of pizzas
    pizzas[numPizzas++] = newpie;
  }
  
  /**
  * PizzaOrder calcCost
  * @return double total Cost of the order
  */
  public double calcCost()
  {
    int i;
    double total=0;
    for (i=0; i<numPizzas; i++)
    {
      if (pizzas[i].getSize()=='s') total+=8;
      else if (pizzas[i].getSize()=='m') total+=10;
      else if (pizzas[i].getSize()=='l') total+=12;
      total+=pizzas[i].getNumToppings();
    }
    return total;
  }
}