class Question2Pizza
{
  
  // ======================
  //     main method
  // ======================
  public static void main(String[] args)
  {
    PizzaOrder order = new PizzaOrder();
    
    // Make a sample order with two pizzas
    order.addPizzaToOrder('m',true,false,false);  // medium pepperoni
    order.addPizzaToOrder('l',false,true,true);  // large sausage & mushrooms
    System.out.println("For two pizzas, one medium with pepperoni and " +
    "one large with sausage and mushrooms, the total is:");
    System.out.println(order.calcCost());     // $11 + $14 = $25
  }
} 